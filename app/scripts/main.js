jQuery(function ($) {
  $('.phone').inputmask({
    mask: '+7(999)-999-99-99',
    showMaskOnHover: false,
  });

  $(document).on('click', 'a[href^="#"]', function (event) {
    event.preventDefault();

    $('html, body').animate(
      {
        scrollTop: $($.attr(this, 'href')).offset().top,
      },
      500
    );
  });

  $('.open-modal').on('click', function (e) {
    e.preventDefault();

    $('.m-season').hide();
    $('.m-modal').toggle();
  });

  $('.m-modal__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'm-modal__centered') {
      $('.m-modal').hide();
    }
  });

  $('.m-modal__close').on('click', function (e) {
    e.preventDefault();
    $('.m-modal').hide();
  });

  $('.open-season').on('click', function (e) {
    e.preventDefault();

    $('.m-season').toggle();
  });

  $('.m-season__centered').on('click', function (e) {
    e.preventDefault();

    if (e.target.className === 'm-season__centered') {
      $('.m-season').hide();
    }
  });

  $('.m-season__close').on('click', function (e) {
    e.preventDefault();
    $('.m-season').hide();
  });

  $('.m-header__toggle').on('click', function (e) {
    e.preventDefault();
    $('.m-header__info').slideToggle('fast');
    $('.m-header__nav').slideToggle('fast');
  });

  $('.m-goods__link').on('click', function (e) {
    e.preventDefault();
    $(this).toggleClass('m-goods__link_active');
    $(this).next().slideToggle('fast');
  });

  $('.m-good__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this)
        .next()
        .val(--currentVal);
    }
  });

  $('.m-good__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this)
        .prev()
        .val(++currentVal);
    }
  });

  $('.m-basket-card__minus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).next().val();

    if (currentVal > 1) {
      $(this)
        .next()
        .val(--currentVal);
    }
  });

  $('.m-basket-card__plus').on('click', function (e) {
    e.preventDefault();

    var currentVal = $(this).prev().val();

    if (currentVal < 99) {
      $(this)
        .prev()
        .val(++currentVal);
    }
  });

  $('.m-tabs__header li').click(function () {
    var tab_id = $(this).attr('data-tab');

    $('.m-tabs__header li').removeClass('current');
    $('.m-tabs__content').removeClass('current');

    $(this).addClass('current');
    $('#' + tab_id).addClass('current');
  });

  new Swiper('.m-top__cards', {
    loop: true,
    spaceBetween: 100,
    navigation: {
      nextEl: '.m-top__cards .swiper-button-next',
      prevEl: '.m-top__cards .swiper-button-prev',
    },
  });

  new Swiper('.m-specs__cards', {
    spaceBetween: 60,
    slidesPerView: 'auto',
    loop: false,
    navigation: {
      nextEl: '.m-specs .swiper-button-next',
      prevEl: '.m-specs .swiper-button-prev',
    },
  });

  new Swiper('.m-papers__cards', {
    spaceBetween: 60,
    slidesPerView: 'auto',
    loop: false,
    navigation: {
      nextEl: '.m-papers .swiper-button-next',
      prevEl: '.m-papers .swiper-button-prev',
    },
  });

  new Swiper('.m-home-reviews__cards', {
    spaceBetween: 30,
    slidesPerView: 1,
    loop: true,
    navigation: {
      nextEl: '.m-home-reviews .swiper-button-next',
      prevEl: '.m-home-reviews .swiper-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 3,
      },
    },
  });

  new Swiper('.m-more__cards', {
    spaceBetween: 0,
    slidesPerView: 1,
    loop: true,
    navigation: {
      nextEl: '.m-more .swiper-button-next',
      prevEl: '.m-more .swiper-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 4,
      },
    },
  });

  new Swiper('.m-another__cards', {
    spaceBetween: 55,
    slidesPerView: 1,
    loop: true,
    navigation: {
      nextEl: '.m-another .swiper-button-next',
      prevEl: '.m-another .swiper-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 2,
      },
      1200: {
        slidesPerView: 4,
      },
    },
  });

  new Swiper('.m-gallery__cards', {
    spaceBetween: 30,
    slidesPerView: 1,
    loop: true,
    navigation: {
      nextEl: '.m-gallery .swiper-button-next',
      prevEl: '.m-gallery .swiper-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 1,
      },
      1200: {
        slidesPerView: 2,
      },
    },
  });

  new Swiper('.m-courses__cards', {
    spaceBetween: 30,
    slidesPerView: 1,
    loop: true,
    navigation: {
      nextEl: '.m-courses .swiper-button-next',
      prevEl: '.m-courses .swiper-button-prev',
    },
    breakpoints: {
      768: {
        slidesPerView: 1,
      },
      1200: {
        slidesPerView: 2,
      },
    },
  });

  $(window).on('load resize', function (e) {
    if ($(window).width() < 1199) {
      $('.m_mob_image').detach().insertAfter('.m-home-what__title');
    }
  });
});
